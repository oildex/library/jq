# jq

[jq](https://stedolan.github.io/jq/) is a lightweight and flexible command-line JSON processor.

This repository extends the official [alpine](https://hub.docker.com/_/alpine) image by adding the jq package.
