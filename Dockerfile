FROM alpine:3.9

ENV JQ_VERSION=1.6-r0

RUN apk add --no-cache jq=$JQ_VERSION

CMD ["/usr/bin/jq", "--help"]

LABEL org.label-schema.url="https://stedolan.github.io/jq/"
